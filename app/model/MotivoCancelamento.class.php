<?php
/**
 * MotivoCancelamento Active Record
 * @author  <your-name-here>
 */
class MotivoCancelamento extends TRecord
{
    const TABLENAME = 'motivo_cancelamento';
    const PRIMARYKEY= 'id';
    const IDPOLICY =  'max'; // {max, serial}
    
    
    /**
     * Constructor method
     */
    public function __construct($id = NULL, $callObjectLoad = TRUE)
    {
        parent::__construct($id, $callObjectLoad);
        parent::addAttribute('motivo');
    }


}
