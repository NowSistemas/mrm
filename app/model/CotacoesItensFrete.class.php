<?php
/**
 * CotacoesItensFrete Active Record
 * @author  <your-name-here>
 */
class CotacoesItensFrete extends TRecord
{
    const TABLENAME = 'cotacoes_itens_frete';
    const PRIMARYKEY= 'id';
    const IDPOLICY =  'max'; // {max, serial}
    
    
    /**
     * Constructor method
     */
    public function __construct($id = NULL, $callObjectLoad = TRUE)
    {
        parent::__construct($id, $callObjectLoad);
        parent::addAttribute('cotacao_id');
        parent::addAttribute('agente_id');
        parent::addAttribute('descricao');
        parent::addAttribute('moeda_id');
        parent::addAttribute('valor');
    }

// Moedas
    public function set_moedas(Moedas $object)
    {
        $this->moedas = $object;
        $this->moeda_id = $object->id;
    }
    
    public function get_moedas()
    {
        if (empty($this->moedas))
            $this->moedas = new Moedas($this->moeda_id);
    
        return $this->moedas;
    }
}
