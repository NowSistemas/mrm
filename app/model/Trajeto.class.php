<?php
/**
 * Trajeto Active Record
 * @author  <your-name-here>
 */
class Trajeto extends TRecord
{
    const TABLENAME = 'trajeto';
    const PRIMARYKEY= 'id';
    const IDPOLICY =  'max'; // {max, serial}
    
    
    /**
     * Constructor method
     */
    public function __construct($id = NULL, $callObjectLoad = TRUE)
    {
        parent::__construct($id, $callObjectLoad);
        parent::addAttribute('origem_id');
        parent::addAttribute('destino_id');
    }

// Origem
    public function set_origem(Origem $object)
    {
        $this->origem = $object;
        $this->id = $object->id;
    }
    
    public function get_origem()
    {
        if (empty($this->origem))
            $this->origem = new Origem($this->origem_id);
    
        return $this->origem;
    }
    
// Destino
    public function set_destino(Origem $object)
    {
        $this->destino = $object;
        $this->id = $object->id;
    }
    
    public function get_destino()
    {
        if (empty($this->destino))
            $this->destino = new Origem($this->destino_id);
    
        return $this->destino;
    }    

}
