<?php
/**
 * Clientes Active Record
 * @author  <your-name-here>
 */
class Clientes extends TRecord
{
    const TABLENAME = 'clientes';
    const PRIMARYKEY= 'id';
    const IDPOLICY =  'max'; // {max, serial}
    
    
    /**
     * Constructor method
     */
    public function __construct($id = NULL, $callObjectLoad = TRUE)
    {
        parent::__construct($id, $callObjectLoad);
        parent::addAttribute('razaosocial');
        parent::addAttribute('nomefantasia');
        parent::addAttribute('cnpj');
        parent::addAttribute('endereco');
        parent::addAttribute('bairro');
        parent::addAttribute('cidade');
        parent::addAttribute('uf');
        parent::addAttribute('telefone1');
        parent::addAttribute('telefone2');
        parent::addAttribute('email');
    }


}
