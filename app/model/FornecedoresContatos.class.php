<?php
/**
 * FornecedoresContatos Active Record
 * @author  <your-name-here>
 */
class FornecedoresContatos extends TRecord
{
    const TABLENAME = 'fornecedores_contatos';
    const PRIMARYKEY= 'id';
    const IDPOLICY =  'max'; // {max, serial}
    
    
    /**
     * Constructor method
     */
    public function __construct($id = NULL, $callObjectLoad = TRUE)
    {
        parent::__construct($id, $callObjectLoad);
        parent::addAttribute('fornecedor_id');
        parent::addAttribute('nome');
        parent::addAttribute('email');
        parent::addAttribute('telefone1');
        parent::addAttribute('telefone2');
    }


}
