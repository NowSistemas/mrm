<?php
/**
 * ClientesContatos Active Record
 * @author  <your-name-here>
 */
class ClientesContatos extends TRecord
{
    const TABLENAME = 'clientes_contatos';
    const PRIMARYKEY= 'id';
    const IDPOLICY =  'max'; // {max, serial}
    
    
    /**
     * Constructor method
     */
    public function __construct($id = NULL, $callObjectLoad = TRUE)
    {
        parent::__construct($id, $callObjectLoad);
        parent::addAttribute('cliente_id');
        parent::addAttribute('nome');
        parent::addAttribute('email');
        parent::addAttribute('telefone1');
        parent::addAttribute('telefone2');
    }


}
