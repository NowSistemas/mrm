<?php
/**
 * Cotacoes Active Record
 * @author  <your-name-here>
 */
class Cotacoes extends TRecord
{
    const TABLENAME = 'cotacoes';
    const PRIMARYKEY= 'id';
    const IDPOLICY =  'max'; // {max, serial}
    
    
    /**
     * Constructor method
     */
    public function __construct($id = NULL, $callObjectLoad = TRUE)
    {
        parent::__construct($id, $callObjectLoad);
        parent::addAttribute('cliente_id');
        parent::addAttribute('data_cotacao');
        parent::addAttribute('modal_id');
        parent::addAttribute('incoterms_id');
        parent::addAttribute('origem_id');
        parent::addAttribute('destino_id');
        parent::addAttribute('endereco_coleta');
        parent::addAttribute('endereco_entrega');
        parent::addAttribute('peso_taxavel');
        parent::addAttribute('total_m3');
        parent::addAttribute('status');
        parent::addAttribute('motivo_cancelamento_id');
        parent::addAttribute('motivo_cancelamento_descricao');
        parent::addAttribute('observacoes');
    }
    
// Clientes
    public function set_cliente(Clientes $object)
    {
        $this->clientes = $object;
        $this->id = $object->id;
    }
    
    public function get_cliente()
    {
        if (empty($this->clientes))
            $this->clientes = new Clientes($this->cliente_id);
    
        return $this->clientes;
    }    

// Origem
    public function set_origem(Origem $object)
    {
        $this->origem = $object;
        $this->id = $object->id;
    }
    
    public function get_origem()
    {
        if (empty($this->origem))
            $this->origem = new Origem($this->origem_id);
    
        return $this->origem;
    }
    
// Destino
    public function set_destino(Destino $object)
    {
        $this->destino = $object;
        $this->id = $object->id;
    }
    
    public function get_destino()
    {
        if (empty($this->destino))
            $this->destino = new Destino($this->destino_id);
    
        return $this->destino;
    }
    
// Modal
    public function set_modal(Modal $object)
    {
        $this->modal = $object;
        $this->id = $object->id;
    }
    
    public function get_modal()
    {
        if (empty($this->modal))
            $this->modal = new Modal($this->modal_id);
    
        return $this->modal;
    }
    
// Incoterms
    public function set_incoterms(Incoterms $object)
    {
        $this->incoterms = $object;
        $this->id = $object->id;
    }
    
    public function get_incoterms()
    {
        if (empty($this->incoterms))
            $this->incoterms = new Incoterms($this->incoterms_id);
    
        return $this->incoterms;
    }    
}
