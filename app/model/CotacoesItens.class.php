<?php
/**
 * CotacoesItens Active Record
 * @author  <your-name-here>
 */
class CotacoesItens extends TRecord
{
    const TABLENAME = 'cotacoes_itens';
    const PRIMARYKEY= 'id';
    const IDPOLICY =  'max'; // {max, serial}
    
    
    /**
     * Constructor method
     */
    public function __construct($id = NULL, $callObjectLoad = TRUE)
    {
        parent::__construct($id, $callObjectLoad);
        parent::addAttribute('cotacao_id');
        parent::addAttribute('quantidade');
        parent::addAttribute('peso_volume');
        parent::addAttribute('comprimento');
        parent::addAttribute('largura');
        parent::addAttribute('altura');
    }


}
