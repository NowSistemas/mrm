<?php
/**
 * Propostas Active Record
 * @author  <your-name-here>
 */
class Propostas extends TRecord
{
    const TABLENAME = 'propostas';
    const PRIMARYKEY= 'id';
    const IDPOLICY =  'max'; // {max, serial}
    
    
    /**
     * Constructor method
     */
    public function __construct($id = NULL, $callObjectLoad = TRUE)
    {
        parent::__construct($id, $callObjectLoad);
        parent::addAttribute('fornecedor_id');
        parent::addAttribute('data_envio');
        parent::addAttribute('hora_envio');
    }


}
