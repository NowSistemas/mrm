<?php
/**
 * Origem Active Record
 * @author  <your-name-here>
 */
class Origem extends TRecord
{
    const TABLENAME = 'origem';
    const PRIMARYKEY= 'id';
    const IDPOLICY =  'max'; // {max, serial}
    
    
    /**
     * Constructor method
     */
    public function __construct($id = NULL, $callObjectLoad = TRUE)
    {
        parent::__construct($id, $callObjectLoad);
        parent::addAttribute('origem');
        parent::addAttribute('descricao');
    }


}
