<?php
/**
 * TrajetoList Listing
 * @author  <your name here>
 */
class TrajetoList extends TPage
{
    protected $form;     // registration form
    protected $datagrid; // listing
    protected $pageNavigation;
    protected $formgrid;
    protected $deleteButton;
    
    use Adianti\base\AdiantiStandardListTrait;
    
    /**
     * Page constructor
     */
    public function __construct()
    {
        parent::__construct();
        
        $this->setDatabase('mrm');            // defines the database
        $this->setActiveRecord('Trajeto');   // defines the active record
        $this->setDefaultOrder('id', 'asc');         // defines the default order
        // $this->setCriteria($criteria) // define a standard filter

        $this->addFilterField('origem_id', '=', 'origem_id'); // filterField, operator, formField
        $this->addFilterField('destino_id', '=', 'destino_id'); // filterField, operator, formField
        
        // creates the form
        $this->form = new BootstrapFormBuilder('form_Trajeto');
        $this->form->setFormTitle('Trajeto');
        

        // create the form fields
        $origem_id  = new TDBUniqueSearch('origem_id', 'mrm', 'Origem', 'id', 'origem');
        $destino_id = new TDBUniqueSearch('destino_id', 'mrm', 'Destino', 'id', 'destino');


        // add the fields
        $this->form->addFields( [ new TLabel('Origem:') ], [ $origem_id ] );
        $this->form->addFields( [ new TLabel('Destino:') ], [ $destino_id ] );


        // set sizes
        $origem_id->setSize('100%');
        $destino_id->setSize('100%');

        
        // keep the form filled during navigation with session data
        $this->form->setData( TSession::getValue('Trajeto_filter_data') );
        
        // add the search form actions
        $btn = $this->form->addAction(_t('Find'), new TAction([$this, 'onSearch']), 'fa:search');
        $btn->class = 'btn btn-sm btn-primary';
        $this->form->addActionLink(_t('New'), new TAction(['TrajetoForm', 'onEdit']), 'fa:plus green');
        
        // creates a DataGrid
        $this->datagrid = new BootstrapDatagridWrapper(new TDataGrid);
        $this->datagrid->style = 'width: 100%';
        $this->datagrid->datatable = 'true';
        // $this->datagrid->enablePopover('Popover', 'Hi <b> {name} </b>');
        

        // creates the datagrid columns
        $column_origem_id  = new TDataGridColumn('origem->descricao', 'Origem', 'left');
        $column_destino_id = new TDataGridColumn('destino->descricao','Destino','left');


        // add the columns to the DataGrid
        $this->datagrid->addColumn($column_origem_id);
        $this->datagrid->addColumn($column_destino_id);

        
        // create EDIT action
        $action_edit = new TDataGridAction(['TrajetoForm', 'onEdit']);
        //$action_edit->setUseButton(TRUE);
        //$action_edit->setButtonClass('btn btn-default');
        $action_edit->setLabel(_t('Edit'));
        $action_edit->setImage('fa:pencil-square-o blue fa-lg');
        $action_edit->setField('id');
        $this->datagrid->addAction($action_edit);
        
        // create DELETE action
        $action_del = new TDataGridAction(array($this, 'onDelete'));
        //$action_del->setUseButton(TRUE);
        //$action_del->setButtonClass('btn btn-default');
        $action_del->setLabel(_t('Delete'));
        $action_del->setImage('fa:trash-o red fa-lg');
        $action_del->setField('id');
        $this->datagrid->addAction($action_del);
        
        // create the datagrid model
        $this->datagrid->createModel();
        
        // create the page navigation
        $this->pageNavigation = new TPageNavigation;
        $this->pageNavigation->setAction(new TAction([$this, 'onReload']));
        $this->pageNavigation->setWidth($this->datagrid->getWidth());
        


        // vertical box container
        $container = new TVBox;
        $container->style = 'width: 100%';
        // $container->add(new TXMLBreadCrumb('menu.xml', __CLASS__));
        $container->add($this->form);
        $container->add(TPanelGroup::pack('', $this->datagrid, $this->pageNavigation));
        
        parent::add($container);
    }
    

}
