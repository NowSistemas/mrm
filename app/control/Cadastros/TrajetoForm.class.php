<?php
/**
 * TrajetoForm Registration
 * @author  <your name here>
 */
class TrajetoForm extends TPage
{
    protected $form; // form
    
    use Adianti\Base\AdiantiStandardFormTrait; // Standard form methods
    
    /**
     * Class constructor
     * Creates the page and the registration form
     */
    function __construct()
    {
        parent::__construct();
        
        $this->setDatabase('mrm');              // defines the database
        $this->setActiveRecord('Trajeto');     // defines the active record
        
        // creates the form
        $this->form = new BootstrapFormBuilder('form_Trajeto');
        $this->form->setFormTitle('Trajetos');
        

        // create the form fields
        $origem_id = new TDBUniqueSearch('origem_id', 'mrm', 'Origem', 'id', 'origem');
        $destino_id = new TDBUniqueSearch('destino_id', 'mrm', 'Origem', 'id', 'origem');


        // add the fields
        $this->form->addFields( [ new TLabel('Origem') ], [ $origem_id ] );
        $this->form->addFields( [ new TLabel('Destino') ], [ $destino_id ] );



        // set sizes
        $origem_id->setSize('100%');
        $destino_id->setSize('100%');


        
        if (!empty($id))
        {
            $id->setEditable(FALSE);
        }
        
        /** samples
         $fieldX->addValidation( 'Field X', new TRequiredValidator ); // add validation
         $fieldX->setSize( '100%' ); // set size
         **/
         
        // create the form actions
        $btn = $this->form->addAction(_t('Save'), new TAction([$this, 'onSave']), 'fa:floppy-o');
        $btn->class = 'btn btn-sm btn-primary';
        $this->form->addAction(_t('New'),  new TAction([$this, 'onEdit']), 'fa:eraser red');
        
        // vertical box container
        $container = new TVBox;
        $container->style = 'width: 100%';
        // $container->add(new TXMLBreadCrumb('menu.xml', __CLASS__));
        $container->add($this->form);
        
        parent::add($container);
    }
}
