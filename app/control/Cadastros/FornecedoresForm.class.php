<?php
class FornecedoresForm extends TPage
{

    protected $form;
    protected $contatos;
        
    public function __construct()
    {
       
        parent::__construct();
        
        $this->form = new BootstrapFormBuilder('form_Fornecedores');
        $this->form->setFormTitle('Cadastro de Fornecedores');
        $this->form->setFieldSizes('100%');
        
        $id             = new THidden('id');
        $razaosocial    = new TEntry('razaosocial');
        $nomefantasia   = new TEntry('nomefantasia');
        $nregistro      = new TEntry('cnpj');
        $endereco       = new TEntry('endereco');
        $bairro         = new TEntry('bairro');
        $cidade         = new TEntry('cidade');
        $uf             = new TEntry('uf');
        $telefone1      = new TEntry('telefone1');
        $telefone2      = new TEntry('telefone2');
        $email          = new TEntry('email');
        
        
        $this->form->appendPage('Cadastro'); 
           
        
        $row = $this->form->addFields( [ new TLabel('<b>N. Registro</b>'), $nregistro ],
                                       [ new TLabel('<b>Razão Social</b>'), $razaosocial ],
                                       [ new TLabel('<b>Nome Fantasia</b>'), $nomefantasia ], 
                                       [ $id ]);
                                       
        $row->layout = ['col-sm-2', 'col-sm-6', 'col-sm-4'];
        
        $row = $this->form->addFields( [ new TLabel('<b>Endereço</b>'), $endereco ],
                                       [ new TLabel('<b>Bairro</b>'), $bairro ], 
                                       [ new TLabel('<b>Cidade</b>'), $cidade ],
                                       [ new TLabel('<b>UF</b>'), $uf ]);
                                       
        $row->layout = ['col-sm-4', 'col-sm-3', 'col-sm-3', 'col-sm-2' ];
        
        $row = $this->form->addFields( [ new TLabel('<b>Telefone #1</b>'), $telefone1 ],
                                       [ new TLabel('<b>Telefone #2</b>'), $telefone2 ], 
                                       [ new TLabel('<b>E-mail</b>'), $email ]);
                                       
        $row->layout = ['col-sm-3', 'col-sm-3', 'col-sm-6' ];
        
// Contatos

        //$this->form->appendPage('Contatos');
        $label = new TLabel('', '#000000', 11,'b');
        $row = $this->form->addContent( [$label] );
        $label = new TLabel('Contatos', '#000000', 11,'b');
        $label->style='text-align:left;border-bottom:1px solid #c0c0c0;width:100%';
        $row = $this->form->addContent( [$label] );        
        
        $nome = new TEntry('nome[]');
        $nome->setSize('100%');
        
        $email_contato = new TEntry('email_contato[]');
        $email_contato->setSize('100%');
        
        $telefone1_contato = new TEntry('telefone1_contato[]');
        $telefone1_contato->setSize('100%');

        $telefone2_contato = new TEntry('telefone2_contato[]');
        $telefone2_contato->setSize('100%');
        
        $this->contatos = new TFieldList;
        $this->contatos->addField( '<b>Nome</b>',        $nome,              ['width' => '20%'] );
        $this->contatos->addField( '<b>E-mail</b>',      $email_contato,     ['width' => '50%'] );        
        $this->contatos->addField( '<b>Telefone #1</b>', $telefone1_contato, ['width' => '15%'] );
        $this->contatos->addField( '<b>Telefone #2</b>', $telefone2_contato, ['width' => '15%'] );
        
        $this->form->addField($nome);
        $this->form->addField($email_contato);
        $this->form->addField($telefone1_contato);
        $this->form->addField($telefone2_contato);
        
        $this->contatos->enableSorting();
        
        $this->form->addContent( [$this->contatos] );
        
        
                
           
        $btn = $this->form->addAction('Salvar', new TAction([$this, 'onSave']), 'fa:floppy-o');
        $btn->class = 'btn btn-sm btn-primary';
        $this->form->addAction('Novo',  new TAction([$this, 'onEdit']), 'bs:plus-sign green');
        $this->form->addAction('Listagem', new TAction(['FornecedoresList','onReload']), 'fa:table blue');
       
        $container = new TVBox;
        $container->style = 'width: 100%';
        $container->add($this->form);
        
        parent::add($container);
        
       
    }
    
    public function onClear( $param )
    {
        $this->contatos->addHeader();
        $this->contatos->addDetail( new stdClass );
        $this->contatos->addCloneAction();
    }    
    
    public static function onSave($param)
    {
        try
        {
            TTransaction::open('mrm');
            
            $id = (int) $param['id'];
            $object = new Fornecedores;
            $object->fromArray( $param);
            $object->cliente_id = TSession::getValue('cliente_id');
            
            $object->store();
            
            new TMessage('info',TSession::getValue('cliente_id'));
            new TMessage('info',$object->cliente_id);
            
            FornecedoresContatos::where('fornecedor_id', '=', $object->id)->delete();
            
            if( !empty($param['nome']) AND is_array($param['nome']) )
            {
                foreach( $param['nome'] as $row => $contato)
                {
                    if ($contato)
                    {
                        $detail = new FornecedoresContatos;
                        $detail->fornecedor_id = $object->id;
                        $detail->nome          = $param['nome'][$row];
                        $detail->email         = $param['email_contato'][$row];
                        $detail->telefone1     = $param['telefone1_contato'][$row];
                        $detail->telefone2     = $param['telefone2_contato'][$row];
                        
                        $detail->store();                    
                    }
                }
            }            
            
            
            $data = new stdClass;
            $data->id = $object->id;
            TForm::sendData('form_Fornecedores', $data);
                        
            TTransaction::close();
            new TMessage('info','Registro salvo com sucesso!');
        }
        catch (Exception $e)
        {
            new TMessage('error', $e->getMessage());
            TTransaction::rollback();
        }    
    }
    
    public function onEdit( $param )
    {

        try
        { 
            TTransaction::open('mrm');
            
            $key = $param['key'];
            
            $object = new Fornecedores( $key );
            $this->form->setData($object);
            
            $icontatos  = FornecedoresContatos::where('fornecedor_id', '=', $key)->load();
                
            if ($icontatos)
            {
               $this->contatos->addHeader();
                    
               foreach($icontatos as $icontato )
               {
                  $detail = new stdClass;
                  $detail->nome = $icontato->nome;
                  $detail->email_contato = $icontato->email;
                  $detail->telefone1_contato = $icontato->telefone1;
                  $detail->telefone2_contato = $icontato->telefone2;
                  
                  $this->contatos->addDetail($detail);
                }
                 
                $this->contatos->addCloneAction();
            }
            else
            {
                $this->onClear($param);
            }
                            
            
            TTransaction::close();    
        }
        catch (Exception $e)
        {
            new TMessage('error', $e->getMessage());
        }    
    }
}
?>
