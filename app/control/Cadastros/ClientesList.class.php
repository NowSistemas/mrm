<?php

class ClientesList extends TStandardList
{
    protected $form;
    protected $datagrid;
    protected $pageNavigation;
    protected $formgrid;
    protected $deleteButton;
    protected $transformCallback;
    
    public function __construct()
    {
        parent::__construct();
        
        parent::setDatabase('mrm');            // defines the database
        parent::setActiveRecord('Clientes');   // defines the active record
        parent::setDefaultOrder('razaosocial', 'asc');         // defines the default order
        parent::addFilterField('id', '=', 'id'); // filterField, operator, formField
        parent::addFilterField('razaosocial', 'like', 'razaosocial'); // filterField, operator, formField
                
        // creates the form
        $this->form = new BootstrapFormBuilder('form_search_clientes');
        $this->form->setFormTitle('Cadastro de Clientes');
        

        // create the form fields
        $cliente = new TEntry('razaosocial');
        
        // add the fields
        $this->form->addFields( [new TLabel('Razão Social')], [$cliente] );
        
        $cliente->setSize('100%');
        
        // keep the form filled during navigation with session data
        $this->form->setData( TSession::getValue('Cliente_filter_data') );
        
        // add the search form actions
        $btn = $this->form->addAction('Pesquisar', new TAction(array($this, 'onSearch')), 'fa:search');
        $btn->class = 'btn btn-sm btn-primary';
        $this->form->addAction('Novo',  new TAction(array('ClientesForm', 'onEdit')), 'bs:plus-sign green');
        
        // creates a DataGrid
        $this->datagrid = new BootstrapDatagridWrapper(new TDataGrid);
        $this->datagrid->datatable = 'true';
        $this->datagrid->style = 'width: 100%';
        $this->datagrid->setHeight(320);
        

        // creates the datagrid columns
        $column_cnpj         = new TDataGridColumn('cnpj', 'No. Registro', 'left');
        $column_razaosocial  = new TDataGridColumn('razaosocial', 'Razão Social', 'left');
        $column_telefone1    = new TDataGridColumn('telefone1', 'Telefone #1', 'left');
        $column_telefone2    = new TDataGridColumn('telefone2', 'Telefone #2', 'left');
        $column_email        = new TDataGridColumn('email', 'E-mail', 'left');
        
        // add the columns to the DataGrid
        $this->datagrid->addColumn($column_cnpj);
        $this->datagrid->addColumn($column_razaosocial);
        $this->datagrid->addColumn($column_telefone1);
        $this->datagrid->addColumn($column_telefone2);
        $this->datagrid->addColumn($column_email);
        
        // creates the datagrid column actions
        $order_cnpj = new TAction(array($this, 'onReload'));
        $order_cnpj->setParameter('order', 'cnpj');
        $column_cnpj->setAction($order_cnpj);
           
        $order_razaosocial = new TAction(array($this, 'onReload'));
        $order_razaosocial->setParameter('order', 'razaosocial');
        $column_razaosocial->setAction($order_razaosocial);
                
        // create EDIT action
        $action_edit = new TDataGridAction(array('ClientesForm', 'onEdit'));
        $action_edit->setButtonClass('btn btn-default');
        $action_edit->setLabel('Alterar');
        $action_edit->setImage('fa:pencil-square-o blue fa-lg');
        $action_edit->setField('id');
        $this->datagrid->addAction($action_edit);
        
        // create DELETE action
        $action_del = new TDataGridAction(array($this, 'onDelete'));
        $action_del->setButtonClass('btn btn-default');
        $action_del->setLabel('Excluir');
        $action_del->setImage('fa:trash-o red fa-lg');
        $action_del->setField('id');
        $this->datagrid->addAction($action_del);
        
        // create the datagrid model
        $this->datagrid->createModel();
        
        // create the page navigation
        $this->pageNavigation = new TPageNavigation;
        $this->pageNavigation->enableCounters();
        $this->pageNavigation->setAction(new TAction(array($this, 'onReload')));
        $this->pageNavigation->setWidth($this->datagrid->getWidth());
        
        $panel = new TPanelGroup;
        $panel->add($this->datagrid);
        $panel->addFooter($this->pageNavigation);
        
        // vertical box container
        $container = new TVBox;
        $container->style = 'width: 100%';
        //$container->add(new TXMLBreadCrumb('menu.xml', __CLASS__));
        $container->add($this->form);
        $container->add($panel);
        
        parent::add($container);
    }
}
