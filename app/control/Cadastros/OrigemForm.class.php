<?php
/**
 * OrigemForm Registration
 * @author  <your name here>
 */
class OrigemForm extends TPage
{
    protected $form; // form
    
    use Adianti\Base\AdiantiStandardFormTrait; // Standard form methods
    
    /**
     * Class constructor
     * Creates the page and the registration form
     */
    function __construct()
    {
        parent::__construct();
        
        $this->setDatabase('mrm');              // defines the database
        $this->setActiveRecord('Origem');     // defines the active record
        
        // creates the form
        $this->form = new BootstrapFormBuilder('form_Origem');
        $this->form->setFormTitle('Origem');
        

        // create the form fields
        $id = new TEntry('id');
        $origem = new TEntry('origem');
        $descricao = new TEntry('descricao');


        // add the fields
        $this->form->addFields( [ new TLabel('Id') ], [ $id ] );
        $this->form->addFields( [ new TLabel('Código') ], [ $origem ] );
        $this->form->addFields( [ new TLabel('Localidade') ], [ $descricao ] );

        $id->setEditable(FALSE);

        // set sizes
        $id->setSize('100%');
        $origem->setSize('100%');
        $descricao->setSize('100%');


       
        /** samples
         $fieldX->addValidation( 'Field X', new TRequiredValidator ); // add validation
         $fieldX->setSize( '100%' ); // set size
         **/
         
        // create the form actions
        $btn = $this->form->addAction(_t('Save'), new TAction([$this, 'onSave']), 'fa:floppy-o');
        $btn->class = 'btn btn-sm btn-primary';
        $this->form->addAction(_t('New'),  new TAction([$this, 'onEdit']), 'fa:eraser red');
        
        // vertical box container
        $container = new TVBox;
        $container->style = 'width: 100%';
        //$container->add(new TXMLBreadCrumb('menu.xml', __CLASS__));
        $container->add($this->form);
        
        parent::add($container);
    }
}
