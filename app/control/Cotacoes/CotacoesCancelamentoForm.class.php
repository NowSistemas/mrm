<?php
/**
 * CotacoesCancelamentoForm Registration
 * @author  <your name here>
 */
class CotacoesCancelamentoForm extends TWindow
{
    protected $form; // form
    
    use Adianti\Base\AdiantiStandardFormTrait; // Standard form methods
    
    function __construct( $param )
    {
        parent::__construct();
        
        $this->setDatabase('mrm');              // defines the database
        $this->setActiveRecord('Cotacoes');     // defines the active record
        
        // creates the form
        $this->form = new BootstrapFormBuilder('form_Cotacoes');
        $this->form->setFormTitle('Cancelar Cotação No. '.$param['id']);
        $this->form->setFieldSizes('100%');
        $this->setSize(0.4,0.45);
        
        

        // create the form fields
        $motivo_cancelamento_id        = new TDBCombo('motivo_cancelamento_id', 'mrm', 'MotivoCancelamento', 'id', 'motivo');
        $motivo_cancelamento_descricao = new TText('motivo_cancelamento_descricao');

        //$motivo_cancelamento_descricao->setSize(100,10);
        
        // add the fields
        $this->form->addFields( [ new TLabel('Motivo:') ], [ $motivo_cancelamento_id ] );
        $this->form->addFields( [ new TLabel('Observação:') ], [ $motivo_cancelamento_descricao ] );
        
        

        // create the form actions
        $btn = $this->form->addAction(_t('Save'), new TAction([$this, 'onEdit']), 'fa:floppy-o');
        $btn->class = 'btn btn-sm btn-primary';
        $this->form->addAction('Voltar',  new TAction(['CotacoesList', 'onReload']), 'fa:table blue');
        
        // vertical box container
        $container = new TVBox;
        $container->style = 'width: 100%';
        // $container->add(new TXMLBreadCrumb('menu.xml', __CLASS__));
        $container->add($this->form);
        
        parent::add($container);
    }
    
    public function onSave($param){
    
        try        {            TTransaction::open('mrm');
            
            $id = (int) $param['id'];
            $key = $param['key'];            $object = new Cotacoes( $key );
            $object->motivo_cancelamento_id = $param['motivo_cancelamento_id'];
            $object->motivo_cancelamento_descricao = $param['motivo_cancelamento_descricao'];            $object->store();
      
            $data = new stdClass;            $data->id = $object->id;
            TForm::sendData('form_Cotacoes', $data);
                                   TTransaction::close();
            new TMessage('info','Registro salvo com sucesso!');        }        catch (Exception $e)        {            new TMessage('error', $e->getMessage());            TTransaction::rollback();        }    
    
    }
}
