<?php
class CotacoesForm extends TPage
{

    protected $form;
    protected $volumes;
        
    public function __construct()
    {
       
        parent::__construct();
        
        $this->form = new BootstrapFormBuilder('form_Cotacoes');
        $this->form->setFormTitle('Cotações');
        $this->form->setFieldSizes('100%');
        
        $id               = new TEntry('id');
        //$cliente_id       = new TDBUniqueSearch('cliente_id', 'mrm', 'Clientes', 'id', 'razaosocial');
        $data_cotacao     = new TDate('data_cotacao');
        $modal            = new TDBCombo('modal_id', 'mrm', 'Modal', 'id', 'modal');
        $incoterms        = new TDBCombo('incoterms_id', 'mrm', 'Incoterms', 'id', 'descricao');
        $origem           = new TDBUniqueSearch('origem_id', 'mrm', 'Origem', 'id', 'descricao');
        $destino          = new TDBUniqueSearch('destino_id', 'mrm', 'Origem', 'id', 'descricao');
        $endereco_coleta  = new TEntry('endereco_coleta');
        $endereco_entrega = new TEntry('endereco_entrega');
        $peso_taxavel     = new TNumeric('peso_taxavel',3,',','.',true);
        $total_m3         = new TNumeric('total_m3',3,',','.',true);
        
        $id->setEditable(FALSE);
        $origem->setMinLength(1);
        $destino->setMinLength(1);
        
        $this->form->appendPage('Cadastro'); 
           
        
        $row = $this->form->addFields( [ new TLabel('<b>N. Cotação</b>'), $id ],
                                       
                                       [ new TLabel('<b>Data</b>'), $data_cotacao ]);
                                       
        $row->layout = ['col-sm-2', 'col-sm-4'];
        
        $row = $this->form->addFields( [ new TLabel('<b>Modal</b>'), $modal ],
                                       [ new TLabel('<b>Incoterms</b>'), $incoterms ], 
                                       [ new TLabel('<b>Origem</b>'), $origem ],
                                       [ new TLabel('<b>Destino</b>'), $destino ]);
                                       
        $row->layout = ['col-sm-3', 'col-sm-3', 'col-sm-3', 'col-sm-3' ];
        
        $row = $this->form->addFields( [ new TLabel('<b>Endereço de Coleta</b>'), $endereco_coleta ]);
                                       
        $row->layout = ['col-sm-12'];
                
        $row = $this->form->addFields( [ new TLabel('<b>Endereço de Entrega</b>'), $endereco_entrega ]);
                                       
        $row->layout = ['col-sm-12'];
        
        
// Volumes

        //$this->form->addContent( ['<h5>Volumes</h5><hr>'] );
        $this->form->appendPage('Volumes');
       
        $quantidade = new TEntry('quantidade[]');
        $quantidade->setSize('100%');
               
        $comprimento = new TNumeric('comprimento[]',3,',','.',true);
        $comprimento->setSize('100%');
        
        $largura = new TNumeric('largura[]',3,',','.',true);
        $largura->setSize('100%');
        
        $altura = new TNumeric('altura[]',3,',','.',true);
        $altura->setSize('100%');

        $peso = new TNumeric('peso[]',3,',','.',true);
        $peso->setSize('100%');
        
        $this->volumes = new TFieldList;
        $this->volumes->addField( '<b>Quantidade</b>' , $quantidade , ['width' => '20%'] );
        $this->volumes->addField( '<b>Comprimento</b>', $comprimento, ['width' => '20%'] );
        $this->volumes->addField( '<b>Largura</b>'    , $largura    , ['width' => '20%'] );        
        $this->volumes->addField( '<b>Altura</b>'     , $altura     , ['width' => '20%'] );
        $this->volumes->addField( '<b>Peso</b>'       , $peso       , ['width' => '20%'] );
        
        $this->form->addField($quantidade);
        $this->form->addField($comprimento);
        $this->form->addField($largura);
        $this->form->addField($altura);
        $this->form->addField($peso);
        
        $this->volumes->enableSorting();
        
        $this->form->addContent( [$this->volumes] );
        
           
        $btn = $this->form->addAction('Salvar', new TAction([$this, 'onSave']), 'fa:floppy-o');
        $btn->class = 'btn btn-sm btn-primary';
        $this->form->addAction('Novo',  new TAction([$this, 'onEdit']), 'bs:plus-sign green');
        $this->form->addAction('Listagem', new TAction(['CotacoesList','onReload']), 'fa:table blue');
       
        $container = new TVBox;
        $container->style = 'width: 100%';
        $container->add($this->form);
        
        parent::add($container);
        
       
    }
    
    public function onClear( $param )
    {
        $this->volumes->addHeader();
        $this->volumes->addDetail( new stdClass );
        $this->volumes->addCloneAction();
    }    
    
    public static function onSave($param)
    {
        try
        {
            TTransaction::open('mrm');
            
            $id = (int) $param['id'];
            $object = new Cotacoes;
            $object->fromArray( $param);
            $object->cliente_id = TSession::getValue('cliente_id');
            $object->status = 'A';
            $object->store();
            
            CotacoesItens::where('cotacao_id', '=', $object->id)->delete();
            
            if( !empty($param['quantidade']) AND is_array($param['quantidade']) )
            {
                foreach( $param['quantidade'] as $row => $item)
                {
                    if ($item)
                    {
                        $detail = new CotacoesItens;
                        $detail->cotacao_id  = $object->id;
                        $detail->quantidade  = $param['quantidade'][$row];
                        $detail->comprimento = $param['comprimento'][$row];
                        $detail->largura     = $param['largura'][$row];
                        $detail->altura      = $param['altura'][$row];
                        $detail->peso_volume = $param['peso'][$row];
                        $detail->store();                    
                    }
                }
            }            
            
            
            $data = new stdClass;
            $data->id = $object->id;
            TForm::sendData('form_Cotacoes', $data);
                        
            TTransaction::close();
            new TMessage('info','Registro salvo com sucesso!');
        }
        catch (Exception $e)
        {
            new TMessage('error', $e->getMessage());
            TTransaction::rollback();
        }    
    }
    
    public function onEdit( $param )
    {

        try
        { 
            TTransaction::open('mrm');
            
            $key = $param['key'];
            
            $object = new Cotacoes( $key );
            $this->form->setData($object);
            
            $ivolumes = CotacoesItens::where('cotacao_id', '=', $key)->load();
                
            if ($ivolumes)
            {
               $this->volumes->addHeader();
                    
               foreach($ivolumes as $ivolume )
               {
                  $detail = new stdClass;
                  $detail->quantidade  = $ivolume->quantidade;
                  $detail->comprimento = $ivolume->comprimento;
                  $detail->largura     = $ivolume->largura;
                  $detail->altura      = $ivolume->altura;
                  $detail->peso        = $ivolume->peso_volume;
                  
                  $this->volumes->addDetail($detail);
                }
                 
                $this->volumes->addCloneAction();
            }
            else
            {
                $this->onClear($param);
            }
                            
            
            TTransaction::close();    
        }
        catch (Exception $e)
        {
            new TMessage('error', $e->getMessage());
        }    
    }
}
?>
