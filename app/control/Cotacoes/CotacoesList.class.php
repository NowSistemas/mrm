<?php
/**
 * CotacoesList Listing
 * @author  <your name here>
 */
class CotacoesList extends TPage
{
    protected $form;     // registration form
    protected $datagrid; // listing
    protected $pageNavigation;
    protected $formgrid;
    protected $deleteButton;
    
    use Adianti\base\AdiantiStandardListTrait;
    
    /**
     * Page constructor
     */
    public function __construct()
    {
        parent::__construct();
        
        $criteria = new TCriteria();
        $criteria->add(new TFilter('cliente_id', '=', TSession::getValue('cliente_id')));
        
        $this->setDatabase('mrm');            // defines the database
        $this->setActiveRecord('Cotacoes');   // defines the active record
        $this->setDefaultOrder('id', 'asc');         // defines the default order
        $this->setCriteria($criteria); // define a standard filter

        $this->addFilterField('data_cotacao', 'like', 'data_cotacao'); // filterField, operator, formField
        $this->addFilterField('status', 'like', 'status'); // filterField, operator, formField
        
        // creates the form
        $this->form = new BootstrapFormBuilder('form_Cotacoes');
        $this->form->setFormTitle('Cotações');
        

        // create the form fields
        $data_cotacao = new TEntry('data_cotacao');
        $status = new TEntry('status');


        // add the fields
        $this->form->addFields( [ new TLabel('Data Cotacao') ], [ $data_cotacao ] );
        $this->form->addFields( [ new TLabel('Status') ], [ $status ] );


        // set sizes
        $data_cotacao->setSize('100%');
        $status->setSize('100%');

        
        // keep the form filled during navigation with session data
        $this->form->setData( TSession::getValue('Cotacoes_filter_data') );
        
        // add the search form actions
        $btn = $this->form->addAction(_t('Find'), new TAction([$this, 'onSearch']), 'fa:search');
        $btn->class = 'btn btn-sm btn-primary';
        $this->form->addActionLink(_t('New'), new TAction(['CotacoesForm', 'onEdit']), 'fa:plus green');
        
        // creates a DataGrid
        $this->datagrid = new BootstrapDatagridWrapper(new TDataGrid);
        $this->datagrid->style = 'width: 100%';
        $this->datagrid->datatable = 'true';
        // $this->datagrid->enablePopover('Popover', 'Hi <b> {name} </b>');
        

        // creates the datagrid columns
        $column_id = new TDataGridColumn('id', 'Id', 'right');
        $column_cliente_id = new TDataGridColumn('cliente->razaosocial', 'Cliente', 'left');
        $column_data_cotacao = new TDataGridColumn('data_cotacao', 'Data', 'center');
        $column_modal_id = new TDataGridColumn('modal->modal', 'Modal', 'left');
        $column_incoterms_id = new TDataGridColumn('incoterms->descricao', 'Incoterms', 'left');
        $column_origem_id = new TDataGridColumn('origem->descricao', 'Origem', 'left');
        $column_destino_id = new TDataGridColumn('destino->descricao', 'Destino', 'left');
        $column_endereco_coleta = new TDataGridColumn('endereco_coleta', 'Endereço Coleta', 'left');
        $column_endereco_entrega = new TDataGridColumn('endereco_entrega', 'Endereço Entrega', 'left');
        $column_peso_taxavel = new TDataGridColumn('peso_taxavel', 'Peso Taxável', 'left');
        $column_total_m3 = new TDataGridColumn('total_m3', 'Total M3', 'left');
        $column_status = new TDataGridColumn('status', 'Status', 'left');


        // add the columns to the DataGrid
        $this->datagrid->addColumn($column_id);
        $this->datagrid->addColumn($column_cliente_id);
        $this->datagrid->addColumn($column_data_cotacao);
        $this->datagrid->addColumn($column_modal_id);
        $this->datagrid->addColumn($column_incoterms_id);
        $this->datagrid->addColumn($column_origem_id);
        $this->datagrid->addColumn($column_destino_id);
        $this->datagrid->addColumn($column_endereco_coleta);
        $this->datagrid->addColumn($column_endereco_entrega);
        $this->datagrid->addColumn($column_peso_taxavel);
        $this->datagrid->addColumn($column_total_m3);
        $this->datagrid->addColumn($column_status);
        
        $column_status->setTransformer( function($value, $object, $row){
        switch ($value){
        case "A": $class = 'primary';
                  $label = 'Aberta';
                  break;
        case "C": $class = 'danger';
                  $label = 'Cancelada';
                  break;                  
        case "F": $class = 'warning';
                  $label = 'Finalizada';
                  break;
        }
        
        $div = new TElement('span');
        $div->class = "label label-{$class}";
        $div->style = "text-shadow: none; font-size: 12px; font-weight: lighter";
        $div->add($label);
        
        return $div;
        });

        // create EDIT action
        $action_edit = new TDataGridAction(['CotacoesForm', 'onEdit']);
        //$action_edit->setUseButton(TRUE);
        //$action_edit->setButtonClass('btn btn-default');
        $action_edit->setLabel(_t('Edit'));
        $action_edit->setImage('fa:pencil-square-o blue fa-lg');
        $action_edit->setField('id');
        $this->datagrid->addAction($action_edit);        
    
        // create DELETE action
        $action_del = new TDataGridAction(array($this, 'onDelete'));
        //$action_del->setUseButton(TRUE);
        //$action_del->setButtonClass('btn btn-default');
        $action_del->setLabel(_t('Delete'));
        $action_del->setImage('fa:trash-o orange fa-lg');
        $action_del->setField('id');
        $this->datagrid->addAction($action_del);  
            
        // Visualizar Propostas 
        $action_propostas = new TDataGridAction(array('PropostasList', 'onEdit'));
        $action_propostas->setButtonClass('btn btn-default');
        $action_propostas->setLabel('Visualizar Propostas');
        $action_propostas->setImage('fa:search fa-lg green');
        $action_propostas->setField('id');
        $this->datagrid->addAction($action_propostas);              
            
        // Cancelar Cotação
        $action_onoff = new TDataGridAction(array('CotacoesCancelamentoForm', 'onEdit'));
        $action_onoff->setButtonClass('btn btn-default');
        $action_onoff->setLabel('Cancelar cotação');
        $action_onoff->setImage('fa:power-off fa-lg red');
        $action_onoff->setField('id');
        $this->datagrid->addAction($action_onoff);                

        
        // create the datagrid model
        $this->datagrid->createModel();
        
        // create the page navigation
        $this->pageNavigation = new TPageNavigation;
        $this->pageNavigation->setAction(new TAction([$this, 'onReload']));
        $this->pageNavigation->setWidth($this->datagrid->getWidth());
        


        // vertical box container
        $container = new TVBox;
        $container->style = 'width: 100%';
        // $container->add(new TXMLBreadCrumb('menu.xml', __CLASS__));
        $container->add($this->form);
        $container->add(TPanelGroup::pack('', $this->datagrid, $this->pageNavigation));
        
        parent::add($container);
    }
    
    public function onTurnOnOff($param)
    {
        try
        {
            TTransaction::open('mrm');
            $cotacao = new Cotacoes($param['id']);
            if ($cotacao instanceof Cotacoes)
            {
                $cotacao->status = 'C';
                $cotacao->store();
            }
            
            TTransaction::close();
            
            $this->onReload($param);
        }
        catch (Exception $e)
        {
            new TMessage('error', $e->getMessage());
            TTransaction::rollback();
        }
    }
      
}
