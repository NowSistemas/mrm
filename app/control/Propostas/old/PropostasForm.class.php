<?php
class PropostasForm extends TPage{    protected $form;
    protected $volumes;
    protected $taxasfretes;
    protected $taxasorigem;
    protected $taxasdestino;
    protected $datagrid_volumes;
    protected $datagrid_frete;
    protected $datagrid_origem;
    protected $datagrid_destino;
        public function __construct()
    {        parent::__construct();
        
        $this->form = new BootstrapFormBuilder('form_Propostas');        $this->form->setFormTitle('Enviar Proposta');        $this->form->setFieldSizes('100%');
        

        $id               = new TEntry('id');
        $cliente_id       = new TEntry('cliente_id');
        $data_cotacao     = new TDate('data_cotacao');
        $modal            = new TDBCombo('modal_id', 'mrm', 'Modal', 'id', 'modal');
        $incoterms        = new TDBCombo('incoterms_id', 'mrm', 'Incoterms', 'id', 'descricao');
        $origem           = new TDBUniqueSearch('origem->descricao', 'mrm', 'Origem', 'id', 'descricao');
        $destino          = new TDBUniqueSearch('destino->descricao', 'mrm', 'Origem', 'id', 'descricao');
        $endereco_coleta  = new TEntry('endereco_coleta');
        $endereco_entrega = new TEntry('endereco_entrega');
        $peso_taxavel     = new TEntry('peso_taxavel');
        $total_m3         = new TEntry('total_m3');
        $obs              = new TText('observacoes');
       
        //$cliente_id->style = "color: blue; font-weight: bold;";
        
        $id->setEditable(FALSE);
        
        //TSession::setValue('id_cotacao',$id);

        $this->form->appendPage('Cotação'); 
        $row = $this->form->addFields( [ new TLabel('<b>N. Cotação</b>'), $id ],
                                       [ new TLabel('<b>Cliente</b>'), $cliente_id ],
                                       [ new TLabel('<b>Data</b>'), $data_cotacao ]);
                                       
        $row->layout = ['col-sm-2', 'col-sm-6', 'col-sm-4'];
        
        $row = $this->form->addFields( [ new TLabel('<b>Modal</b>'), $modal ],
                                       [ new TLabel('<b>Incoterms</b>'), $incoterms ], 
                                       [ new TLabel('<b>Origem</b>'), $origem ],
                                       [ new TLabel('<b>Destino</b>'), $destino ]);
                                       
        $row->layout = ['col-sm-3', 'col-sm-3', 'col-sm-3', 'col-sm-3' ];
        
        $row = $this->form->addFields( [ new TLabel('<b>Endereço de Coleta</b>'), $endereco_coleta ]);
                                       
        $row->layout = ['col-sm-12'];
                
        $row = $this->form->addFields( [ new TLabel('<b>Endereço de Entrega</b>'), $endereco_entrega ]);
                                       
        $row->layout = ['col-sm-12'];
        
        
// Volumes
        $label = new TLabel('', '#000000', 11,'b');
        $row = $this->form->addContent( [$label] );
        $label = new TLabel('Volumes', '#000000', 11,'b');
        $label->style='text-align:left;border-bottom:1px solid #c0c0c0;width:100%';
        $row = $this->form->addContent( [$label] );
                //$this->form->appendPage('Volumes');
        
        // creates a DataGrid
        $this->datagrid_volumes = new TDataGrid;
        $this->datagrid_volumes->style = 'width: 100%';
        
        // creates the datagrid columns
        $quantidade     = new TDataGridColumn('quantidade', 'Quantidade', 'center');
        $comprimento    = new TDataGridColumn('comprimento', 'Comprimento (cm)', 'center');
        $largura        = new TDataGridColumn('largura', 'Largura (cm)', 'center');
        $altura         = new TDataGridColumn('altura', 'Altura (cm)', 'center');
        $peso           = new TDataGridColumn('peso_volume', 'Peso (Kg)', 'center');
        

        // add the columns to the DataGrid
        $this->datagrid_volumes->addColumn($quantidade);
        $this->datagrid_volumes->addColumn($comprimento);
        $this->datagrid_volumes->addColumn($largura);
        $this->datagrid_volumes->addColumn($altura);
        $this->datagrid_volumes->addColumn($peso);
        
        $this->datagrid_volumes->createModel();
        
        $this->form->addContent([$this->datagrid_volumes]);
                        
      
       
        
// Taxas de Frete

        $label = new TLabel('', '#000000', 11,'b');
        $row = $this->form->addContent( [$label] );
        $label = new TLabel('Taxas de Fretes', '#000000', 11,'b');
        $label->style='text-align:left;border-bottom:1px solid #c0c0c0;width:100%';
        $row = $this->form->addContent( [$label] );
        
        // create an new button (edit with no parameters)
        $new_button=new TButton('new');
        $new_button->setAction(new TAction(array('PropostaTaxaFrete', 'onEdit')), 'Nova Taxa de Frete');
        $new_button->setImage('ico_new.png');
        $new_button->class = 'btn btn-sm btn-primary';
        $new_button->style = 'float:right';
        //$new_button->setField('id');
                
        $this->form->setFields([$new_button]);        
                // creates a DataGrid
        $this->datagrid_frete = new TDataGrid;
        $this->datagrid_frete->style = 'width: 100%';
        
        // creates the datagrid columns
        $descricao = new TDataGridColumn('descricao', 'Descrição', 'left');
        $moeda     = new TDataGridColumn('moedas->moeda', 'Moeda', 'center');
        $valor     = new TDataGridColumn('valor', 'Valor', 'center');
        

        // add the columns to the DataGrid
        $this->datagrid_frete->addColumn($descricao);
        $this->datagrid_frete->addColumn($moeda);
        $this->datagrid_frete->addColumn($valor);
        
       // create EDIT action
       $edit_frete = new TDataGridAction(['PropostaTaxaFrete', 'onEdit']);
       $edit_frete->setUseButton(TRUE);
       $edit_frete->setButtonClass('btn btn-default');
       $edit_frete->setLabel('Alterar');
       $edit_frete->setImage('fa:pencil-square-o blue fa-lg');
       $edit_frete->setField('id');
       $this->datagrid_frete->addAction($edit_frete);         
        
       // create DELETE action
       $del_frete = new TDataGridAction([$this, 'onDeleteFrete']);
       $del_frete->setUseButton(TRUE);
       $del_frete->setButtonClass('btn btn-default');
       $del_frete->setLabel('Excluir');
       $del_frete->setImage('fa:trash-o red fa-lg');
       $del_frete->setField('id');
       $this->datagrid_frete->addAction($del_frete);        
        
       $this->datagrid_frete->createModel();
       $this->form->addContent([$this->datagrid_frete]);
       $this->form->addContent([$new_button]);


// Taxas de Origem

        $label = new TLabel('', '#000000', 11,'b');
        $row = $this->form->addContent( [$label] );
        $label = new TLabel('Taxas na Origem', '#000000', 11,'b');
        $label->style='text-align:left;border-bottom:1px solid #c0c0c0;width:100%';
        $row = $this->form->addContent( [$label] );
        
        // create an new button (edit with no parameters)
        $new_button_taxa_origem=new TButton('new');
        $new_button_taxa_origem->setAction(new TAction(array('PropostaTaxaOrigem', 'onEdit')), 'Nova Taxa de Origem');
        $new_button_taxa_origem->setImage('ico_new.png');
        $new_button_taxa_origem->class = 'btn btn-sm btn-primary';
        $new_button_taxa_origem->style = 'float:right';
                
        $this->form->setFields([$new_button_taxa_origem]);
                

        // creates a DataGrid
        $this->datagrid_origem = new TDataGrid;
        $this->datagrid_origem->style = 'width: 100%';
        
        // creates the datagrid columns
        $descricao_o = new TDataGridColumn('descricao', 'Descrição', 'left');
        $moeda_o     = new TDataGridColumn('moedas->moeda', 'Moeda', 'center');
        $valor_o     = new TDataGridColumn('valor', 'Valor', 'center');
        

        // add the columns to the DataGrid
        $this->datagrid_origem->addColumn($descricao_o);
        $this->datagrid_origem->addColumn($moeda_o);
        $this->datagrid_origem->addColumn($valor_o);
        
       // create EDIT action
       $edit_origem = new TDataGridAction(['PropostaTaxaOrigem', 'onEdit']);
       $edit_origem->setUseButton(TRUE);
       $edit_origem->setButtonClass('btn btn-default');
       $edit_origem->setLabel('Alterar');
       $edit_origem->setImage('fa:pencil-square-o blue fa-lg');
       $edit_origem->setField('id');
       $this->datagrid_origem->addAction($edit_origem);         
        
       // create EDIT action
       $del_origem = new TDataGridAction(['PropostaTaxaOrigem', 'onDelete']);
       $del_origem->setUseButton(TRUE);
       $del_origem->setButtonClass('btn btn-default');
       $del_origem->setLabel('Excluir');
       $del_origem->setImage('fa:trash-o red fa-lg');
       $del_origem->setField('id');
       $this->datagrid_origem->addAction($del_origem);        
        
       $this->datagrid_origem->createModel();
       $this->form->addContent([$this->datagrid_origem]);
       $this->form->addContent([$new_button_taxa_origem]);       
               
// Taxas no Destino

        $label = new TLabel('', '#000000', 11,'b');
        $row = $this->form->addContent( [$label] );
        $label = new TLabel('Taxas no Destino', '#000000', 11,'b');
        $label->style='text-align:left;border-bottom:1px solid #c0c0c0;width:100%';
        $row = $this->form->addContent( [$label] );
        
        // create an new button (edit with no parameters)
        $new_button_taxa_destino=new TButton('new');
        $new_button_taxa_destino->setAction(new TAction(array('PropostaTaxaDestino', 'onEdit')), 'Nova Taxa de Destino');
        $new_button_taxa_destino->setImage('ico_new.png');
        $new_button_taxa_destino->class = 'btn btn-sm btn-primary';
        $new_button_taxa_destino->style = 'float:right';
                
        $this->form->setFields([$new_button_taxa_destino]);        

        // creates a DataGrid
        $this->datagrid_destino = new TDataGrid;
        $this->datagrid_destino->style = 'width: 100%';
        
        // creates the datagrid columns
        $descricao_d = new TDataGridColumn('descricao', 'Descrição', 'left');
        $moeda_d     = new TDataGridColumn('moedas->moeda', 'Moeda', 'center');
        $valor_d     = new TDataGridColumn('valor', 'Valor', 'center');
        

        // add the columns to the DataGrid
        $this->datagrid_destino->addColumn($descricao_d);
        $this->datagrid_destino->addColumn($moeda_d);
        $this->datagrid_destino->addColumn($valor_d);
        
       // create EDIT action
       $edit_destino = new TDataGridAction(['PropostaTaxaDestino', 'onEdit']);
       $edit_destino->setUseButton(TRUE);
       $edit_destino->setButtonClass('btn btn-default');
       $edit_destino->setLabel('Alterar');
       $edit_destino->setImage('fa:pencil-square-o blue fa-lg');
       $edit_destino->setField('id');
       $this->datagrid_destino->addAction($edit_destino);         
        
       // create EDIT action
       $del_destino = new TDataGridAction(['PropostaTaxaDestino', 'onDelete']);
       $del_destino->setUseButton(TRUE);
       $del_destino->setButtonClass('btn btn-default');
       $del_destino->setLabel('Excluir');
       $del_destino->setImage('fa:trash-o red fa-lg');
       $del_destino->setField('id');
       $this->datagrid_destino->addAction($del_destino);        
        
       $this->datagrid_destino->createModel();
       $this->form->addContent([$this->datagrid_destino]);
       $this->form->addContent([$new_button_taxa_destino]);       
          
// Observações
        $label = new TLabel('', '#000000', 11,'b');
        $row = $this->form->addContent( [$label] );
        $label = new TLabel('Observações', '#000000', 11,'b');
        $label->style='text-align:left;border-bottom:1px solid #c0c0c0;width:100%';
        $row = $this->form->addContent( [$label] );
        
        $row = $this->form->addFields( [ new TLabel(''), $obs ]);
        $row->layout = ['col-sm-12'];
        
        
        
                
        $btn = $this->form->addAction('Salvar', new TAction([$this, 'onSave']), 'fa:floppy-o');        $btn->class = 'btn btn-sm btn-primary';
        
        $btn_confirm = $this->form->addAction('Confirmar Proposta', new TAction([$this, 'enviarEmail']), 'fa:edit-o');        $btn_confirm->class = 'btn btn-sm btn-success';   
        
        $this->form->addAction('Voltar', new TAction(['CotacoesAgentesList','onReload']), 'fa:table blue');     
        $container = new TVBox;        $container->style = 'width: 100%';        $container->add($this->form);               parent::add($container);       
    }
    
    public function onSave($param)
    {        try        {            TTransaction::open('mrm');
            
            $id = (int) $param['id'];
            $key = $param['key'];            $object = new Cotacoes( $key );
            $object->observacoes = $param['observacoes'];
            $object->store();
            /*            
            CotacoesItensFrete::where('cotacao_id', '=', $object->id)->delete();                        if( !empty($param['descricao_frete']) AND is_array($param['descricao_frete']) )            {                foreach( $param['descricao_frete'] as $row => $item)                {                    if ($item)                    {                        $detail = new CotacoesItensFrete;                        $detail->cotacao_id  = $id;                        $detail->descricao   = $param['descricao_frete'][$row];
                        $detail->moeda_id    = $param['moeda_frete'][$row];
                        $detail->valor       = $param['valor_frete'][$row];
                        $detail->store();                                        }                }            }            */           
      
            $data = new stdClass;            $data->id = $object->id;
            TForm::sendData('form_Cotacoes', $data);
                                   TTransaction::close();
            new TMessage('info','Registro salvo com sucesso!');        }        catch (Exception $e)        {            new TMessage('error', $e->getMessage());            TTransaction::rollback();        }        }       public function onEdit( $param )        {
        try
        {
            TTransaction::open('mrm');
            
            $key = $param['id'];

            $object = new Cotacoes($key); 
            //$object->data_cotacao = TDate::date2br($object->data_cotacao);
            $this->form->setData($object);
            

            $criteria = new TCriteria();
            $criteria->add(new TFilter('cotacao_id',   '=', $key)); 
            
        
            $ivolumes = CotacoesItens::where('cotacao_id', '=', $key)->load();
            $this->datagrid_volumes->addItems($ivolumes);
            
            $itaxasfretes = CotacoesItensFrete::where('cotacao_id', '=', $key)->load();
            $this->datagrid_frete->addItems($itaxasfretes);
            
            $itaxasorigem = CotacoesItensOrigem::where('cotacao_id', '=', $key)->load();
            $this->datagrid_origem->addItems($itaxasorigem);  
                      
            $itaxasdestino = CotacoesItensDestino::where('cotacao_id', '=', $key)->load();
            $this->datagrid_destino->addItems($itaxasdestino);    
                
                        TTransaction::close();            }        catch (Exception $e)        {           new TMessage('error', $e->getMessage());
           TTransaction::rollback();        }        }
    
    public function onEditFrete( $param )    {
    }

    public function onDeleteDestino( $param )    {
    }    
    
    public function onEditDestino( $param )    {
    }
    
    public function enviarEmail($param)
    {
    
        $resposta = new TQuestion('Confirma o envio da proposta?',$acaoYes,$acaoNo,'Confirmação do envio da Proposta');
        
/*        
        $email = 'juliolemos@nowsistemas.com.br';
        //$protocolo = $param['protocolo'];//Número do protocolo da solicitação
        
        try {
            $mail_template = file_get_contents('app/resources/confirmacao_email.html');
            $mail = new TMail;
            
            $mail->setFrom('juliolemos@nowsistemas.com.br', 'MRM Freight');
            $mail->setSubject('Proposta enviada');
            //$mail_template = str_replace('{PROTOCOLO}', $protocolo, $mail_template);
            $mail->setHtmlBody($mail_template);
            $mail->addAddress($email, 'NOME DO AGENTE');
            $mail->SetUseSmtp();
            $mail->SMTPSecure = 'ssl';
            $mail->SMTPAuth = true;
            $mail->SetSmtpHost('seu_host', 'porta');
            $mail->SetSmtpUser('usuario', 'senha');
            $mail->send();
        } catch(Exception $e) {
            new TMessage('error', 'Não foi possível enviar seu e-mail.');
        }
*/        
    }    

    public function onDeleteFrete($param)
    {
        $action = new TAction([__CLASS__, 'DeleteFrete']);
        $action->setParameters($param);
        
        new TQuestion(TAdiantiCoreTranslator::translate('Do you really want to delete ?'), $action);
    }
    
    public function DeleteFrete($param)
    {
        try
        {
            $key=$param['key']; 
            TTransaction::open('mrm'); 
//            $object = new CotacoesItensFrete($key, FALSE);
//            $object->delete();
            
            CotacoesItensFrete::where('id', '=', $key)->delete();
            
            TTransaction::close();
            
            $pos_action = new TAction([__CLASS__, 'onReload']);
            new TMessage('info', TAdiantiCoreTranslator::translate('Record deleted'), $pos_action);
        }
        catch (Exception $e)
        {
            new TMessage('error', $e->getMessage());
            TTransaction::rollback();
        }
    }    
}
?>
