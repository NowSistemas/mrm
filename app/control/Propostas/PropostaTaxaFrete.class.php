<?php
class PropostaTaxaFrete extends TWindow
{

    protected $form;
    protected $contatos;
        
    public function __construct( $param )
    {
       
        parent::__construct();
        
        $this->form = new BootstrapFormBuilder('form_Taxa_Frete');
        $this->form->setFormTitle('Taxas de Frete');
        $this->form->setFieldSizes('100%');
        $this->setSize(0.3999,0.3999);
        
        $id             = new THidden('id');
        $cotacao_id     = new THidden('cotacao_id');
        $agente_id      = new THidden('agente_id');
        $descricao      = new TEntry('descricao');
        $moeda_id       = new TDBCombo('moeda_id', 'mrm', 'Moedas', 'id', 'moeda');
        $valor          = new TNumeric('valor',2,',','.',false);       
        
      
        
        $row = $this->form->addFields( [ new TLabel('<b>Descrição</b>'), $descricao ],
                                       [ new TLabel('<b>Moeda</b>'), $moeda_id ],
                                       [ new TLabel('<b>Valor</b>'), $valor ], 
                                       [ $id ]);
                                       
        $row->layout = ['col-sm-6', 'col-sm-3', 'col-sm-3'];
        
           
        $btn = $this->form->addAction('Salvar', new TAction([$this, 'onSave']), 'fa:floppy-o');
        $btn->class = 'btn btn-sm btn-primary';
        $btn->style = 'float:right';
        //$this->form->addAction('Voltar', new TAction(['PropostasForm','onEdit']), 'fa:table blue');
       
        $container = new TVBox;
        $container->style = 'width: 100%';
        $container->add($this->form);
        
        parent::add($container);
        
       
    }
    
    public static function onReload($param)
    {
        $this->onReload( func_get_arg(0) );
    } 
    
    public static function onSave($param)
    {
        try
        {
            TTransaction::open('mrm');
            
            $id = (int) $param['id'];
            $object = new CotacoesItensFrete;
            $object->id         = $param['id'];
            $object->cotacao_id = TSession::getValue('id_cotacao');
            $object->agente_id  = TSession::getValue('userid');
            $object->descricao  = $param['descricao'];
            $object->moeda_id   = $param['moeda_id'];
            $object->valor      = $param['valor'];
            $object->store();
            
            $data = new stdClass;
            $data->id = $object->id;
            TForm::sendData('form_Taxa_Frete', $data);
                        
            TTransaction::close();
            new TMessage('info','Registro salvo com sucesso!');
            
        }
        catch (Exception $e)
        {
            new TMessage('error', $e->getMessage());
            TTransaction::rollback();
        }    
    }
    
    public function onEdit( $param )
    {

        try
        { 
            TTransaction::open('mrm');
            
            $key = $param['key'];
            
            $object = new CotacoesItensFrete( $key );
            $this->form->setData($object);
            
                          
            
            TTransaction::close();    
        }
        catch (Exception $e)
        {
            new TMessage('error', $e->getMessage());
        }    
    }
    
    public function onDelete($param)
    {
        $action = new TAction([__CLASS__, 'Delete']);
        $action->setParameters($param);
        
        new TQuestion(TAdiantiCoreTranslator::translate('Do you really want to delete ?'), $action);
    }
    
    public function Delete($param)
    {
        try
        {
            $key=$param['key']; 
            TTransaction::open('mrm'); 
            $object = new CotacoesItensFrete($key, FALSE);
            $object->delete();
            
            //CotacoesItensFrete::where('id', '=', $key)->delete();
            
            TTransaction::close();
            
            $pos_action = new TAction([__CLASS__, 'onReload']);
            new TMessage('info', TAdiantiCoreTranslator::translate('Record deleted'), $pos_action);
        }
        catch (Exception $e)
        {
            new TMessage('error', $e->getMessage());
            TTransaction::rollback();
        }
    }    
}
?>
